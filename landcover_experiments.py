import re
import os
import os.path as osp
import argparse
import joblib
import pandas as pd
import geopandas as gpd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
from sklearn.ensemble import RandomForestClassifier
from eumap.mapper import LandMapper
import multiprocessing as mc
from sklearn.model_selection import train_test_split
from sklearn.metrics import classification_report
import itertools

# Names of LUCAS classes
lc1_to_label = {
    'A':'UNKNOWN',
    'A11':'Buildings with 1 to 3 floors',
    'A12':'Buildings with more than 3 floors',
    'A13':'Greenhouses',
    'A21':'Non built-up area features',
    'A22':'Non built-up linear features',
    'A30':'Other artificial areas',
    'B':'UNKNOWN',
    'B11':'Common wheat',
    'B12':'Durum wheat',
    'B13':'Barley',
    'B14':'Rye',
    'B15':'Oats',
    'B16':'Maize',
    'B17':'Rice',
    'B18':'Triticale',
    'B19':'Other cereals',
    'B21':'Potatoes',
    'B22':'Sugar beet',
    'B23':'Other root crops',
    'B31':'Sunflower',
    'B32':'Rape and turnip rape',
    'B33':'Soya',
    'B34':'Cotton',
    'B35':'Other fibre and oleaginous crops',
    'B36':'Tobacco',
    'B37':'Other non-permanent industrial crops',
    'B41':'Dry pulses',
    'B42':'Tomatoes',
    'B43':'Other fresh vegetables',
    'B44':'Floriculture and ornamental plants',
    'B45':'Strawberries',
    'B51':'Clovers',
    'B52':'Lucerne',
    'B53':'Other leguminous and mixtures for fodder',
    'B54':'Mixed cereals for fodder',
    'B55':'Temporary grasslands',
    'B71':'Apple fruit',
    'B72':'Pear fruit',
    'B73':'Cherry fruit',
    'B74':'Nuts trees',
    'B75':'Other fruit trees and berries',
    'B76':'Oranges',
    'B77':'Other citrus fruit',
    'B81':'Olive groves',
    'B82':'Vineyards',
    'B83':'Nurseries',
    'B84':'Permanent industrial crops',
    'BX1':'Arable land (only PI)',
    'BX2':'Permanent crops (only PI)',
    'C':'UNKNOWN',
    'C10':'Broadleaved woodland',
    'C20':'Coniferous woodland',
    'C21':'Spruce dominated coniferous woodland',
    'C22':'Pine dominated coniferous woodland',
    'C23':'Other coniferous woodland',
    'C30':'Mixed woodland',
    'C31':'Spruce dominated mixed woodland',
    'C32':'Pine dominated mixed woodland',
    'C33':'Other mixed woodland',
    'D':'UNKNOWN',
    'D01':'UNKNOWN',
    'D02':'UNKNOWN',
    'D10':'Shrubland with sparse tree cover',
    'D20':'Shrubland without tree cover',
    'E':'UNKNOWN',
    'E01':'UNKNOWN',
    'E02':'UNKNOWN',
    'E10':'Grassland with sparse tree/shrub cover',
    'E20':'Grassland without tree/shrub cover',
    'E30':'Spontaneously vegetated surfaces',
    'F':'UNKNOWN',
    'F00':'Bare land',
    'F10':'Rocks and stones',
    'F20':'Sand',
    'F30':'Lichens and moss',
    'F40':'Other bare soil',
    'G':'UNKNOWN',
    'G01':'UNKNOWN',
    'G02':'UNKNOWN',
    'G03':'UNKNOWN',
    'G05':'UNKNOWN',
    'G10':'Inland water bodies',
    'G11':'Inland fresh water bodies',
    'G12':'Inland salty water bodies',
    'G20':'Inland running water',
    'G21':'Inland fresh running water',
    'G22':'Inland salty running water',
    'G30':'Transitional water bodies',
    'G40':'Marine sea',
    'G50':'Glaciers, permanent snow',
    'H':'UNKNOWN',
    'H11':'Inland marshes',
    'H12':'Peatbogs',
    'H21':'Salt marshes',
    'H22':'Salines and other chemical deposits',
    'H23':'Intertidal flats',
    'C11':'Broadleaved and evergreen woodland - Boreal forest',
    'C12':'Broadleaved and evergreen woodland - Hemiboreal forest and nemoral coniferous forest and mixed broadleaved-coniferous forest',
    'C13':'Broadleaved and evergreen woodland - Alpine coniferous forest',
    'C14':'Broadleaved and evergreen woodland - Acidophilous oak and oak-birch forest',
    'C15':'Broadleaved and evergreen woodland - Mesophytic deciduous forest',
    'C16':'Broadleaved and evergreen woodland - Beech forest',
    'C17':'Broadleaved and evergreen woodland - Mountainous beech forest',
    'C18':'Broadleaved and evergreen woodland - Thermophilous deciduous forest',
    'C19':'Broadleaved and evergreen woodland - Broadleaved evergreen forest',
    'C1A':'Broadleaved and evergreen woodland - Coniferous forest of the Mediterranean region',
    'C1B':'Broadleaved and evergreen woodland - Mire and swamp forests',
    'C1C':'Broadleaved and evergreen woodland - Floodplain forest',
    'C1D':'Broadleaved and evergreen woodland - Non-riverine alder, birch or aspen forest',
    'C1E':'Broadleaved and evergreen woodland - Plantations and self-sown exotic forest',
    'C21':'Coniferous woodland - Boreal forest',
    '8':'UNKNOWN'}

lucas_1_to_label = {
    'A':'Artificial',
    'B':'Crops',
    'C':'Trees',
    'D':'Shrubs',
    'E':'Grass',
    'F':'Bare',
    'G':'Water',
    'H':'Wetlands'
}

lucas_1_to_code = {
    'A':0,
    'B':1,
    'C':2,
    'D':3,
    'E':4,
    'F':5,
    'G':6,
    'H':7
}

if __name__ == '__main__':
    p = argparse.ArgumentParser()
    p.add_argument('--path_points',default='lucas_s2.joblib')
    p.add_argument('--path_reports',default='accuracy_reports.joblib')
    p.add_argument('--test',action='store_true')
    p.add_argument('--retrain',action='store_true')
    p.add_argument('--remove_intermediate',action='store_true')
    a = p.parse_args()

    random_state = np.random.RandomState(420)
   
    # Initialize paths of intermediate files 
    path_lucas_s2_overlaid = a.path_points    
    dir_points = osp.split(path_lucas_s2_overlaid)[0]
    path_lucas_s2_preprocessed = osp.join(dir_points,'lucas_s2_preprocessed.joblib')
    path_lucas_ls_overlaid = osp.join(dir_points,'lucas_ls.joblib')
    path_lucas_ls_preprocessed = osp.join('lucas_ls_preprocessed.joblib')
    path_lucas_train_valid = osp.join('lucas_train_valid.joblib')

    # Create a list of strings that identify the different experiments
    # 30 and 10 indicate resolution in m
    # l = landsat, s = sentinel, l_s = landsat and sentinel
    # ls_big and ls_small is the longer landsat time series.
    models = ['l30','s30','s10','s','30','l_s','ls_big','ls_small']
    experiments = [''.join(i) for i in itertools.product(models,['','_dtm'])]
    print('experiments:\n',experiments)

    def drop(df,term):
        return df[df.columns.drop(list(df.filter(regex=term)))]

    def add_codes(df):
        df['lucas_3_label'] = np.vectorize(lc1_to_label.__getitem__)(df.lc1)
        df = df.loc[df.lucas_3_label != "UNKNOWN"]
        df['lucas_1'] = df.lc1.str[0]
        df['lucas_2'] = df.lc1.str[:2]
        df['lucas_3'] = df.lc1.str[:3]
        df['lucas_1_label'] = np.vectorize(lucas_1_to_label.__getitem__)(df.lucas_1)
        df['lucas_1_code'] = np.vectorize(lucas_1_to_code.__getitem__)(df.lucas_1)
        return df

    def clean(df,exclude=[
        'osm_p','coast','_010m_eu','clm_lst','hyd_surface','green.tri','longitude','latitude',
        'ndvi','evi','savi','msavi','ndmi','nbr','nbr2','rei','ndwi',
        'xl','yl','xu','yu','offst_y','offst_x','rgn_dm_y','rgn_dm_x','.qa_p50_30m_']):
        for ex in exclude:
            df = drop(df,ex)
        
        # Remove LUCAS points that miss data in a relevant variable
        df = df.dropna()
        return df


    def preprocess(path_in,path_out):
        if os.path.exists(path_out):
            df = joblib.load(path_out)

        else:
            df = joblib.load(path_in)
            df = add_codes(df)
            df = clean(df)
            joblib.dump(df,path_out,compress='lz4')
        
        return df


    s2 = preprocess(path_lucas_s2_overlaid,path_lucas_s2_preprocessed)
    ls = preprocess(path_lucas_ls_overlaid,path_lucas_ls_preprocessed)


    # Create a train and test dataset
    if os.path.exists(path_lucas_train_valid):
        s2_train, ls_train_small, ls_train_big, valid = joblib.load(path_lucas_train_valid)
    else:
        s2_train, valid = train_test_split(s2,test_size=0.1,random_state=random_state)
        ls_train_small = ls.sample(len(s2_train))
        ls_train_big = ls.drop(valid.index)
        joblib.dump([s2_train,ls_train_small,ls_train_big,valid],path_lucas_train_valid)

    print("Sentinel-2 points:")
    print(s2_train.survey_year.value_counts())

    print("Mixed-year Landsat points (total should match total number of Sentinel-2 points):")
    print(ls_train_small.survey_year.value_counts())

    print("Landsat points for all years (total should be much more):")
    print(ls_train_big.survey_year.value_counts())

    # Make different point sets by removing unused landsat and/or sentinel variables
    pts_s10_dtm = drop(s2_train,'30m_0..0cm_.')
    pts_l30_dtm = drop(s2_train,'sentinel')
    pts_s30_dtm = drop(drop(s2_train,'landsat'),'10m_0..0cm_.')
    pts_s_dtm = drop(s2_train,'landsat')
    pts_30_dtm = drop(s2_train,'10m')

    pts_s10 = drop(pts_s10_dtm,'dtm')
    pts_s30 = drop(pts_s30_dtm,'dtm')
    pts_l30 = drop(pts_l30_dtm,'dtm')
    pts_s = drop(pts_s_dtm,'dtm')
    pts_30 = drop(pts_30_dtm,'dtm')

    # Make the landmappers
    def make_landmapper(pts,
        prefixes=['lcv','dtm'],
        target='lucas_1_code',
        cv=5,
        cores=mc.cpu_count()-1,
        threshold=0.001):
        print("Making landmapper with",len(pts),'training points')
        estimator=RandomForestClassifier(n_estimators=100,n_jobs=cores,random_state=random_state)
        lm = LandMapper(
            points=pts,feat_col_prfxs=prefixes,target_col=target,
            estimator=estimator,min_samples_per_class=threshold,
            cv=cv,cv_njobs=cores)
        return lm

    if a.test:
        path_landmapper_test = 'test_landmapper.joblib'
        if os.path.exists(path_landmapper_test) and not a.retrain:
            print(f"Loading test model from {path_landmapper_test}")
            lm = LandMapper.load_instance(path_landmapper_test)
            
        else:
            print(f"Training landmapper and saving it at {path_landmapper_test}")    
            lm = make_landmapper(s2_train)
            lm.train()
            lm.save_instance(path_landmapper_test)

    else:
        lm_l30_dtm = make_landmapper(pts_l30_dtm)
        lm_s30_dtm = make_landmapper(pts_s30_dtm)
        lm_s10_dtm = make_landmapper(pts_s10_dtm)
        lm_l30 = make_landmapper(pts_l30)
        lm_s30 = make_landmapper(pts_s30)
        lm_s10 = make_landmapper(pts_s10)
        lm_s_dtm = make_landmapper(pts_s_dtm)
        lm_s = make_landmapper(pts_s)
        lm_30_dtm = make_landmapper(pts_30_dtm)
        lm_30 = make_landmapper(pts_30)
        lm_l_s_dtm = make_landmapper(s2_train)
        lm_l_s = make_landmapper(drop(s2_train,'dtm'))
        
        lm_ls_small_dtm = make_landmapper(ls_train_small)
        lm_ls_big_dtm = make_landmapper(ls_train_big)
        lm_ls_small = make_landmapper(drop(ls_train_small,'dtm'))
        lm_ls_big = make_landmapper(drop(ls_train_big,'dtm'))

        # Train the landmappers
        for experiment in experiments:
            eval(f'lm_{experiment}').train()

    # Export accuracy assessments as Tex tables
    def to_latex(df,id,dir_reports):
        t = df.to_latex(float_format="%.3f")
        with open(f"{dir_reports}{os.sep}tab_accuracy_report_{id}.tex",'w') as f:
            f.write(t)

    def report(true,pred):
        r = pd.DataFrame(classification_report(true,pred,output_dict=True)).T
        r = r.astype('float')
        r = r.astype({'support':'int32'})

        return r

    def make_report(landmapper,id,valid=None,dir_reports='tables'):
        os.makedirs(dir_reports,exist_ok=True)
        # r = landmapper.eval_report
        r_cv = report(landmapper.target,landmapper.eval_pred)
        print(r_cv)
        to_latex(r_cv,f"{id}_cv",dir_reports)

        if valid is not None:
            print("validating")
            pred = landmapper.predict_points(valid)[0]
            true = valid.lucas_1_code

            r_valid = report(true,pred)
            print(r_valid)
            to_latex(r_valid,f"{id}_test",dir_reports)
            return [r_cv,r_valid]
        return r_cv

    reports = {}

    if a.test:
        make_report(lm,'test_all',valid)

    else:
        for experiment in experiments:
            reports[f"{experiment}"] = make_report(
                landmapper=eval(f"lm_{experiment}"),id=f"{experiment}",valid=valid)

        joblib.dump(reports,path_reports)
        
    if a.remove_intermediate:
        for path_intermediate in [
            path_lucas_s2_preprocessed, path_lucas_ls_overlaid, 
            path_lucas_ls_preprocessed, path_lucas_train_valid]:
            
            os.remove(path_intermediate)
        
        

