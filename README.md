# EcoDataCube
This repository contains Python code used in the paper **EcoDataCube.eu: Analysis-Ready Open Environmental Data Cube for Europe** which is currently in review([v1](https://assets.researchsquare.com/files/rs-2277090/v3/dd792f6e233646d52bbadc8e.pdf?c=1670514213),v2 forthcoming). The code uses the [eumap](https://eumap.readthedocs.io/en/latest/) python package for overlaying satellite data on land cover samples and training machine learning models that can be used for mapping.

## Land cover experiments
- `overlay_lucas_s2.py` overlays a dataset of LUCAS points with Sentinel-2 data.
- `analysis_landcover.py` trains several random forests on the overlaid points and generates accuracy reports, which are stored in an accuracy_reports.joblib file.
- `landcover_f1_analysis.ipynb` is a jupyter notebook which loads the accuracy reports and visualizes how the models perform when trained on certain parts of the feature space presented in the EcoDataCube: Landsat, Sentinel-2 and DTM.


please direct any questions to martijn.witjes@opengeohub.org
