import geopandas as gpd
import joblib
import numpy as np
import pandas as pd
import os
import argparse

from eumap.mapper import SpaceTimeOverlay

# This script overlays LUCAS observations with Sentinel-2 data

if __name__ == '__main__':
    p = argparse.ArgumentParser()
    p.add_argument('--path_lucas',default='pts_lucas_all.joblib')
    p.add_argument('--path_overlaid',default='points_lucas_s2.joblib')
    a = p.parse_args()

lucas = joblib.load(a.path_lucas)

# Example of sentinel 30m:
# https://s3.eu-central-1.wasabisys.com/eumap/lcv/lcv_swir1_sentinel.s2l2a_p50_30m_0..0cm_2017.12.02..2018.03.20_eumap_epsg3035_v1.0.tif

# Example of sentinel 10m:
# https://s3.eu-central-1.wasabisys.com/eumap/lcv/lcv_blue_sentinel.s2l2a_p75_10m_0..0cm_2017.03.21..2017.12.01_eumap_epsg3035_v1.0.tif

url = 'https://s3.eu-central-1.wasabisys.com/eumap/lcv/lcv_'

bands_10m = ['red','green','blue','nir']
bands_30m = bands_10m + ['swir1','swir2']
quartiles = [
    '{year_minus_1}.12.02..{year}.03.20',
    '{year}.03.21..{year}.06.24',
    '{year}.06.25..{year}.09.12',
    '{year}.09.13..{year}.12.01'
]
percentiles = ['p25','p50','p75']

tifs = []

suffix = '_eumap_epsg3035_v1.0.tif'
satellite = '_sentinel.s2l2a_'

for percentile in percentiles:
    for band in bands_10m:
        tifs.append(url + band + satellite + percentile + '_10m_0..0cm_{year}.03.21..{year}.12.01' + suffix)
    for band in bands_30m:
        for quartile in quartiles:
            tifs.append(url + band + satellite + percentile + '_30m_0..0cm_' + quartile + suffix)
for tif in tifs:
    print(tif)

print(len(lucas))
print(lucas.survey_year.value_counts())
lucas = lucas[lucas.survey_year.astype(int) > 2016]
print(len(lucas))
print(lucas.survey_year.value_counts())
# lucas = lucas.sample(100)
overlay = SpaceTimeOverlay(points=lucas, col_date='survey_year',fn_layers=tifs, verbose=True)
lucas = overlay.run()

joblib.dump(lucas,a.path_overlaid,compress='lz4')
